//有一个已经排好序的数组。现输入一个数，要求按原来的规律将它插入数组中。
//判断数和数组中其中某一项的大小，如果可以插进去，就比如插入11,11<14，那就把这一项替代后一项，并把后一项的数保存，然后从这个数开始依次把数存入进去。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main(void)
{
	int b = 0;
	int a[11] = { 1,2,4,7,10,14,16,19,23,31 };
	printf("请输入一个要插入的数:");
	scanf("%d", &b);
	int i = 0, j = 0,t=0,temp=0;
	if (b >= a[9])
		a[10] = b;
	else
	{
		for (i = 0; i < 10; i++)
		{
			if (b < a[i])
			{
				t = a[i];
				a[i] = b;
				break;
			}
		}
		for (j = i + 1; j < 11; j++)
		{
			temp = a[j];
			a[j] = t;
			t = temp;
		}
	}
	for (i = 0; i < 11; i++)
		printf("%-3d", a[i]);
}