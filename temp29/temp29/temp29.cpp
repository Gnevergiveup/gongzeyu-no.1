//利用递归函数调用方式，将所输入的5个字符，以相反顺序打印出来。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void pilot(int n)
{
	char next;
	if (n >= 2)
	{
		next = getchar();
		pilot(n - 1);
		putchar(next);//当n大于等于2时一直递归
	}
	if(n<=1)
	{
		next = getchar();
		printf("按相反顺序输出");
		putchar(next);//当n小宇等于1直接输出
	}
}
int main()
{
	int i = 5;
	printf("请输入5个字符\40:\40");
	pilot(i);
	printf("\n");
}