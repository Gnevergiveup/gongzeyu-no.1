//输入两个正整数m和n，求其最大公约数和最小公倍数。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int main()
//{
//	int m=0, n=0;
//	printf("please input m,n\n");
//	scanf("%d%d",&n,&m);
//	int i = (n >m ? m: n);
//	for (i ; i >= 1; i--)
//	{
//		if (m % i == 0)
//		{
//			if (n % i == 0)
//			{
//				printf("%d", i);
//				break;
//			}
//		}
//	}
//	return 0;
//}
//#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//int main()
//{
//	int x = 0, y = 0;
//	printf("请输入两个数字：");
//	scanf("%d%d", &x, &y);
//	int tmp = x < y ? x : y; //把两个数的最小值赋给tmp
//	while (1)
//	{
//		if (x % tmp == 0 && y % tmp == 0)
//		{
//			break;  //找到最大公约数了，跳出四循环
//		}
//		tmp--;  //两个数都不能整除，自减1
//	}
//	printf("最大公约数是：%d", tmp);
//	return 0;
//}

//更相减损法：
//int main()
//{
//	int m, n;
//	printf("please input m,n\n");
//	scanf("%d%d",&n,&m);
//	while (n != m)
//	{
//		if (n > m)
//		{
//			n = n - m;
//		}
//		if (n < m)
//		{
//			m = m - n;
//		}
//	}
//	printf("%d", m);
//	return 0;
//}


// 递归写法:
//int fun(int x, int y)
//{
//	if (x > y)
//		return fun(y, x - y);
//	else if (y > x)
//		return fun(x, y - x);
//	else
//		return x;
//}
//int main()
//{
//	int x = 0, y = 0;
//	printf("please input x,y\n");
//	scanf("%d%d",&x,&y);
//	int f = fun(x, y);
//	printf("最大公约数：%d", f);
//	return 0;
//}

//最小公倍数：

//暴力法
//int main()
//{
//	int x = 0, y = 0;
//	printf("please input x,y\n");
//	scanf("%d%d",&x,&y);
//	int i = x > y ? x : y;
//	while (1)
//	{
//		if (i % x == 0 && i % y == 0)
//			break;
//		else
//			i++;
//	}
//	printf("最小公倍数：%d", i);
//}



// 公式法
//int main()
//{
//	int x = 0, y = 0;
//    printf("please input x,y\n");
//    scanf("%d%d",&x,&y);
//    int tem1 = x, tem2 = y;
//    while (x != y)
//    {
//        if (x > y)
//        {
//            x = x - y;
//        }
//        if (y > x)
//        {
//            y = y - x;
//        }
//    }
//    int i = 0;
//    i = tem1 * tem2 / x;
//    printf("最小公倍数：%d", i);
//}
