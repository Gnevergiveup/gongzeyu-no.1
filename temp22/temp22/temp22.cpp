//有一分数序列：2 / 1，3 / 2，5 / 3，8 / 5，13 / 8，21 / 13...求出这个数列的前20项之和。
#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{
//	float a = 1.0, b = 2.0, c = 3.0;
//	float x = 0.0, y = 0.0, z = 0.0,t=0.0;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		x = b / a;
//		y = c / b;
//		t = b;
//		b = b + c;//b=5
//		a = a + t;//a=3
//		c = a + b;
//		z += (x + y);
//	}
//	printf("%f", z);
//}

//方法二

#include <stdio.h>

int main()
{
    int i, t;
    float sum = 0;
    float a = 2, b = 1;
    for (i = 1; i <= 20; i++)
    {
        sum = sum + a / b;
        t = a;
        a = a + b;
        b = t;
    }
    printf("%9.6f\n", sum);
}