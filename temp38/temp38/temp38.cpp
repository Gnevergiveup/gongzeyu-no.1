//有一个已经排好序的数组。现输入一个数，要求按原来的规律将它插入数组中。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int a[11] = { 1,2,3,4,5,7,8,9,10,11 };
	int i = 0,j=0;
	for (i = 0; i < 10; i++)
	{
		printf("%d", a[i]);
	}
	printf("现在插入一个数：");
	int b;
	scanf("%d", &b);
	int end = a[9],temp1,temp2;
	if (b > end)
		a[10] = b;
	else
	{
		for (i = 0; i < 10; i++)
		{
			if (b < a[i])
			{
				temp1 = a[i];
				a[i] = b;
				for (j = i+1; j < 11; j++)
				{
					temp2 = a[j];
					a[j] = temp1;
					temp1 = temp2;
				}
				break;
			}
		}
	}
	for (i = 0; i < 11; i++)
		printf("%4d", a[i]);
	printf("\n");
	return 0;
}