//古典问题（兔子生崽）：
//有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，
//假如兔子都不死，问每个月的兔子总数为多少？（输出前40个月即可）

#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int i=2, j=2;//i,j是第一，二月的兔子总数
	int a = 0;
	for (a = 0; a < 20; a++)
	{
		printf("%12d%12d", i, j);
		if (a % 2 == 0)
			printf("\n");
		i = i + j;
		j = i + j;
		
	}
}