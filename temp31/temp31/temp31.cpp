//给一个不多于5位的正整数，
//要求：一、求它是几位数，二、逆序打印出各位数字。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int n = 0;
	printf("请输入一个不多于5位的正整数");
	scanf("%d", &n);
	int a=0, b=0, c=0, d=0, e=0;
	a = n / 10000;//
	b = n / 1000 % 10;
	c = n / 100 % 10;
	d = n / 10 % 10;
	e = n % 10;
	if (n / 10000 != 0)
		printf("五位数，%d %d %d %d %d", e, d, c, b, a);
	else if(n/1000!=0)
		printf("四位数，%d %d %d %d", e, d, c, b);
	else if(n/100!=0)
		printf("三位数，%d %d %d", e, d, c);
	else if(n/10!=0)
		printf("俩位数，%d %d", e, d);
	else if(e!=0)
		printf("一位数，%d", e);
	return 0;
}