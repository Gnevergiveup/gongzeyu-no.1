//有5个人坐在一起，问第五个人多少岁？他说比第4个人大2岁。问第4个人岁数，他说比第3个人大2岁。
//问第三个人，又说比第2人大两岁。问第2个人，说比第一个人大两岁。最后问第一个人，他说是10岁。请问第五个人多大？
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int age(int x);
int main()
{
	int n = 5;
	printf("%d", age(n));
	return 0;
}
int age(int x)
{
	int c;
	if (x == 1)
	{
		c = 10;
	}
	else
	{
		c = age(x - 1) + 2;
	}
	return c;
}