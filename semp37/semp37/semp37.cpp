//对10个数进行排序
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main(void)
{
	int a[10];
	int i = 0, j = 0,t=0;
	printf("please input 10 number");
	for (i = 0; i < 10; i++)
	{
		scanf("%d", &a[i]);
	}
	for (i = 0; i < 9; i++)
	{
		for (j = i; j < 10; j++)
		{
			if (a[i] < a[j])
			{
				t = a[i];
				a[i] = a[j];
				a[j] = t;
			}
		}
	}
	for (i = 0; i < 10; i++)
	{
		printf("%2d", a[i]);
	}
}