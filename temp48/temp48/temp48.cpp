//文件的打开、关闭、fgetc和fputc的知识点
#define _CRT_SECURE_NO_WARNINGS 1
//1.如何确定文件是否正常打开
//2读取字符一定要使打开文件切换成可读
FILE* fp;
if ((fp = fopen("a.txt", "w")) == NULL)
{
	printf("cannot open this file");
	exit(0)//头文件stdlib.h
}
