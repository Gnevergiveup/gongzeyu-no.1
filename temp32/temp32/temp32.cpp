//一个5位数，判断它是不是回文数。即12321是回文数，个位与万位相同，十位与千位相同。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int n = 0;
	printf("输入一个5位数\n");
	scanf("%d", &n);
	int a, b, c, d;
	a = n / 10000;
	b = n / 1000 % 10;
	c = n / 10 % 10;
	d = n % 10;
	if (a == d && b == c)
		printf("是回文数");
	else
		printf("不是回文数");
	return 0;
}