//学习使用如何调用外部函数。分析下面程序
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int a, b, c;
void add()
{
	int a;
	a = 3;
	c = a + b;
}
int main()
{
	a = b = 4;
	add();
	printf("c 的值为 %d\n", c);
	return 0;
}
//c=7