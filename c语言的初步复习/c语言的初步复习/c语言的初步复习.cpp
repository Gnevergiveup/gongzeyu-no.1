//常量，变量，运算符与表达式，运算符的优先级和结合性
/*输入字符getchar()，输出字符putchar()
* 格式化输入输出：scanf和printf
* 选择结构：if else  
* switch
* {
* case 1:printf()
* }
* 循环
* goto
* while
* do
* {
* }while;()
*/
#include<stdio.h>
int main()
{
	int i = 0;
	int a[10] = { 0 };
	for (i = 0; i < 9; i++)
	{
		a[i + 1] = a[i] + 1;
		printf("%d",a[i]);

	}
	return 0;
}