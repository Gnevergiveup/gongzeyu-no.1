//一球从100米高度自由落下，每次落地后反跳回原高度的一半；
//再落下，求它在第10次落地时，共经过多少米？第10次反弹多高？
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	float a = 100.0, b = 0.0,j = 0.0;//b是总米数
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		a /= 2;
		b = (a * 2 + a);
		j += b;
	}
	printf("共经过：%f米", j);
	printf("第10次反弹：%f", a);
	return 0;
}