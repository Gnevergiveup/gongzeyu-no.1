//fputc和fgetc的综合练习
//要求：以只写打开文件。
//如果打开成功，则输入一个字符到变量ch中。
//使用fputc函数，将字符ch中字符写入fp指针所指向的文件。
//再读入一个字符到ch。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
int main(void)
{
	FILE* fp;
	fp = fopen("b.txt", "w");
	if ((fp == NULL))
	{
		printf("cannot open this file");
		exit(1);
	}
	char ch;
	ch = getchar();
	fputc(ch, fp);
	fclose(fp);
	fp = fopen("b.txt", "r");
	ch = fgetc(fp);
	putchar(ch);
	fclose(fp);
}