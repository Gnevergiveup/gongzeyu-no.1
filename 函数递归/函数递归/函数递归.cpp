#define _CRT_SECURE_NO_WARNINGS
//递归要求
//1.需要设置停下来的条件
// 2.需要不断逼近与停下来的条件

// 递归的特点，大事化小，代码使用量少
//函数递归举例
//1.求n的阶乘
/*
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int fact(int n)
{
	if (n == 0)
	{
		return 1;
	}
	else
		return n * fact(n - 1);
}
int main()
{
	int n = 0;
	scanf("%d",&n);
	int ret = fact(n);
	printf("%d\n", ret);
	return 0;
}
*/
//2.顺序打印一个整数的每一位
#include<stdio.h>
/*
	if (n > 9)//1234
	{
		print(n / 10);

	}
	printf("%d ", n % 10);
*/
void print(int n)
{
	if (n > 9)//1234
	{
		print(n / 10);//123
		printf("%d ", n % 10);//4
	}
	else
	{
		printf("%d ", n % 10);
	}
}
int main()
{
	int n = 0;
	scanf("%d",&n);
	print(n);
	return 0;
}