//有一个数学函数，当 - 100 < x < 0时y = 5x, 当x = 0时y = -1, 当0 < x < 10时y = 2x + 1, 编写一个应用程序，输入x值时，计算并输出y值
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int main(void)
//{
//	int x = 0, y = 0;
//	scanf("%d", &x);
//	if (x > -100 && x < 0)
//		y = 5 * x;
//	else if (x == 0)
//		y = -1;
//	else if (x > 0 && x < 10)
//		y = 2*x+ 1;
//	printf("%d", y);
//}


//输出1 - 100中能被7整除的数，并统计其个数。
//int main(void)
//{
//	int i = 0, t = 0;
//	for (i = 1; i < 100; i++)
//	{
//		if (i % 7 == 0)
//		{
//			printf("%-4d", i);
//			t++;
//		}
//	}
//	printf("\n");
//	printf("%d", t);
//}


//编程，从键盘输入两个整数，输出两个数中的大数。比较数的大小用函数实现。
//int sum(int x, int y)
//{
//	if (x >= y)
//		return x;
//	else
//		return y;
//}
//int main(void)
//{
//	int a = 0, b = 0;
//	scanf("%d%d", &a, &b);
//	printf("%d", sum(a, b));
//}


//从键盘输入10个整数，用冒泡排序法将它们按从大到小的次序排列后输出。
int main(void)
{
	int a[10];
	int i = 0, j = 0,t = 0;
	int* p = a;
	for (i = 0; i < 10; i++)
		scanf("%d", p + i);
	for (i = 0; i < 9; i++)
	{
		for (j = i + 1; j < 10; j++)
		{
			if (*(p + i) < *(p + j))
			{
				t = *(p + i);
				*(p + i) = *(p + j);
				*(p + j) = t;
			}
		}
	}
	for (i = 0; i < 10; i++)
		printf("%-3d", *(p + i));
}