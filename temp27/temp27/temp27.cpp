//利用自定义参数实现三个数比大小
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void sum()
{
	int a[3],i=0,j=0;
	int t = 0;
	for (i = 0; i < 3; i++)
		scanf("%d", &a[i]);
	for (i = 0; i < 2; i++)
	{
		for (j = i+1; j < 3; j++)
		{
			if (a[i] > a[j])
			{
				t = a[i];
				a[i] = a[j];
				a[j] = t;
			}
		}
	}
	printf("%d<%d<%d", a[0], a[1], a[2]);
}
int main()
{
	sum();
	return 0;
}
	