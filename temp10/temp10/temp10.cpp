//将一个正整数分解质因数。例如：输入90, 打印出90 = 2 * 3 * 3 * 5。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int a = 0;
	printf("please input a number\n");
	scanf("%d",&a);
	int i=0;
	for (i = 2; i <= a; i++)
	{
		while (a % i == 0)
		{
			printf("%d", i);
			a /= i;
			if (a != 1)
				printf("*");
		}
	}
	return 0;
}