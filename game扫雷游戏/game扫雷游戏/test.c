#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"
void menu()
{
	printf("****************************\n");
	printf("***********1. play**********\n");
	printf("***********0. exit**********\n");
	printf("****************************\n");
}

void game()
{
	//扫雷游戏的实现
	char mine[ROWS][COLS] = { 0 };
	char show[ROWS][COLS] = { 0 };
	//初始化函数
	Initboard(mine , ROWS , COLS , '0');
	Initboard(show , ROWS , COLS , '*');//初始化棋盘

	//布置雷9*9的棋盘上随机布置10个雷
	SetMine(mine, ROW, COL);
	
	//打印棋盘
	//DisplayBorad(mine, ROW, COL);
	DisplayBorad(show, ROW, COL);

	//排查雷
	FindMine(mine, show, ROW, COL);
}

void test()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择：-->");
		scanf("%d", &input);//1 0 x
		switch (input)
		{
		case 1:
			game();
			printf("扫雷\n");
			break;
		case 0:
			printf("游戏结束，退出游戏\n");
			break;
		default:
			printf("选择错误，重新选择\n");
			break;
		}
	} while (input);
}

int main()
{
	test();
	return 0;
}