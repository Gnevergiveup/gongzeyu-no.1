//将一个数组逆序输出
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main(void)
{
	int a[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int* p = a;
	int i = 0,t=0;
	for (i = 0; i < 5; i++)
	{
		t = *(p + i);
		*(p + i) = *(p + 9 - i);
		*(p + 9 - i) = t;
	}
	for (i = 0; i < 10; i++)
		printf("%-3d", *(p + i));
}