//求1 + 2!+ 3!+ ... + 20!的和。
#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{
//	int a=0;
//	int i = 0, j = 0;
//	for (i = 1; i < 21; i++)
//	{
//		int b = 1;
//		for (j = 1; j <=i; j++)
//		{
//			b *= j;
//		}
//		a += b;
//	}
//	printf("%d", a);
//}

//  Created by www.runoob.com on 15/11/9.
//  Copyright ? 2015年 菜鸟教程. All rights reserved.
//

#include <stdio.h>

int main()
{
    int i;
    long double sum, mix;
    sum = 0, mix = 1;
    for (i = 1; i <= 20; i++)
    {
        mix = mix * i;
        sum = sum + mix;
    }
    printf("%Lf\n", sum);
}