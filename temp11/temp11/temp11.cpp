//利用条件运算符的嵌套来完成此题：学习成绩 >= 90分的同学用A表示，
//60 - 89分之间的用B表示，60分以下的用C表示。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int n;
	printf("please input grades\n");
	scanf("%d", &n);
	if (n >= 90)
		printf("A");
	else if (n >= 60 && n <= 89)
		printf("B");
	else if (n < 60)
	    printf("C");
	return 0;
}