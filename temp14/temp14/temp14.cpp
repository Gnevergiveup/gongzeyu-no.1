//求s = a + aa + aaa + aaaa + aa...a的值，其中a是一个数字。
//例如2 + 22 + 222 + 2222 + 22222(此时共有5个数相加)，几个数相加有键盘控制。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int a = 0, n = 0;
	printf("please input a and n\n");
	scanf("%d%d", &a, &n);
	int i = 0,j=0,t=0,s=0,tem=0;
	for (j = 0; j < n; j++)
	{
		tem = a;
		for (i = 0; i < j; i++)
		{
			a *= 10;
		}
		t += a;//t=1 t=11
		s += t;//s=1 s=12
		a = tem;
	}
	printf("s=a+aa+aaa……=%d", s);
	return 0;
}