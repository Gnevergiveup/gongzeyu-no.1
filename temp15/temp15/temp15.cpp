//一个数如果恰好等于它的因子之和，这个数就称为"完数"。
// 例如6 = 1＋2＋3.编程找出1000以内的所有完数。
//#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{
//	int n = 1000, i = 0, j = 0;
//	int s = 0,m=0;
//	for (i = 2; i < n; i++)
//	{
//		for (j = 2; j < i; j++)
//		{
//			if (i % j == 0)
//			{
//				s += j;
//			}
//		}
//		if (s + 1 == i)
//		{
//			printf("%5d", i);
//			m++;
//		}
//		if (m % 5 == 0)
//			printf("\0");
//		s = 0;
//	}
//	return 0;
//}

//数组法
#include<stdio.h>
#define N 1000
int main()
{
	int i = 0, j = 0, sum=0,n=0;
	int a[300];
	for (i = 2; i <= N; i++)
	{
		int k = 0;
		sum = a[0] = 1;
		for (j = 2; j <= (i / 2); j++)
		{
			if (i % j == 0)
			{
				sum += j;
				a[++k] = j;
			}
		}
		if (i == sum)
		{
			printf("%d=%d", i, a[0]);
			for (n = 1; n <= k; n++)
				printf("+%d", a[n]);
			printf("\n");
		}
	}
	return 0;
}