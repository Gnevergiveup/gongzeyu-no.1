//题目：三个数找最大数
//1.有参数，2有返回值，3无参数，4无返回值
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//1有参数 有返回值
//int sum(int a, int b, int c)
//{
//	if (a > b)
//	{
//		if (a > c)
//			return a;
//		else
//			return c;
//	}
//	if (a < b)
//	{
//		if (b > c)
//			return b;
//		else
//			return c;
//	}
//}
//int main()
//{
//	int a=0, b=0, c=0;
//	printf("please input a,b,c\n");
//	scanf("%d%d%d", &a, &b, &c);
//	printf("sum = %d", sum(a,b,c));
//}


//有参数无返回值
//void sum(int a[], int n)
//{
//	int i=0,j=0,t=0;
//	for (i = 0; i < n-1; i++)
//	{
//		for(j=i+1;j<n;j++)
//			if (a[i] < a[j])
//			{
//				t = a[i];
//				a[i] = a[j];
//				a[j] = t;
//			}
//	}
//}
//int main()
//{
//	int a[3],i=0;
//	printf("please input a,b,c\n");
//	for (i = 0; i < 3; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	sum(a, 3);
//	printf("%d", a[0]);
//}


//3无参数有返回值
//int sum()
//{
//	int a[3], i = 0;
//	printf("please input a,b,c\n");
//	for (i = 0; i < 3; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	int  j = 0,t=0 ;
//	for (i = 0; i < 2; i++)
//	{
//		for (j = i + 1; j < 3; j++)
//			if (a[i] < a[j])
//			{
//				t = a[i];
//				a[i] = a[j];
//				a[j] = t;
//			}
//	}
//	return a[0];
//}
//int main()
//{
//	printf("%d", sum());
//}

//4无参数无返回值
void sum()
{
	int a[3], i = 0;
	printf("please input a,b,c\n");
	for (i = 0; i < 3; i++)
	{
		scanf("%d", &a[i]);
	}
	int  j = 0,t=0 ;
	for (i = 0; i < 2; i++)
	{
		for (j = i + 1; j < 3; j++)
			if (a[i] < a[j])
			{
				t = a[i];
				a[i] = a[j];
				a[j] = t;
			}
	}
	printf("%d", a[0]);
}
int main()
{
	sum();
}
