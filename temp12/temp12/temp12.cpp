//输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	char c;
	int letter=0, spaces=0, number=0, others=0;
	while ((c = getchar()) != '\n')
	{
		if ((c >= 'a' && c <= 'z')||(c >= 'A' && c <= 'Z'))
			letter++;
		else if (c >= '0' && c <= '9')
			number++;
		else if (c == ' ')
			spaces++;
		else
			others++;
	}
	printf("%d,%d,%d,%d", letter, spaces, number, others);
	return 0;
}