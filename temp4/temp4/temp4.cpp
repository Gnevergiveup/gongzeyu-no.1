//输入某年某月某日，判断这一天是这一年的第几天？
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int day = 0, month = 0, year = 0, sum = 0;
	printf("请输入年，月，日");
	scanf("%d,%d,%d", &year, &month, &day);
	switch (month)//计算月份以前的天数
	{
	case(1):sum = 0; break;
	case(2):sum = 31; break;
	case(3):sum = 59; break;
	case(4):sum = 90; break;
	case(5):sum = 120; break;
	case(6):sum = 151; break;
	case(7):sum = 181; break;
	case(8):sum = 212; break;
	case(9):sum = 243; break;
	case(10):sum = 273; break;
	case(11):sum = 304; break;
	case(12):sum = 334; break;
	default:printf("error");
	}
	sum = sum + day;
	if (year % 4 == 0 && year % 100 != 0)
	{
		if (month <= 2)
			sum = sum;
		if (month >= 3)
			sum += 1;
	}
	if (year % 1000 == 0 && year % 400 == 0)
	{
		if (month <= 2)
			sum = sum;
		if (month >= 3)
			sum += 1;
	}
	printf("%d", sum);
}