//打印出如下图案（菱形）
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int main()
//{
//	int i = 0, j = 0;
//	int a = 0, b = 0;
//	int x = 3, y = 1;
//	for (i = 0; i < 4; i++)
//	{
//		for (a = 0; a < x; a++)
//		{
//			printf(" ");
//		}
//		x--;
//		for (b = 0; b < y; b++)
//		{
//			if (y > 7)
//				break;
//			printf("*");
//		}
//		y += 2;
//		printf("\n");
//	}
//	int m = 0, n = 0;
//	int c = 1, d = 5;
//	for (i = 0; i < 3; i++)
//	{
//		for (m = 0; m < c; m++)
//		{
//			if (c > 3)
//				break;
//			printf(" ");
//		}
//		c++;
//		for (n = 0; n < d; n++)
//		{
//			if (d < 1)
//				break;
//			printf("*");
//		}
//		d -= 2;
//		printf("\n");
//	}
//}


//简便方法
int main()
{
    int i, j, k;
    for (i = 0; i <= 3; i++) {
        for (j = 0; j <= 2 - i; j++) {
            printf(" ");
        }
        for (k = 0; k <= 2 * i; k++) {
            printf("*");
        }
        printf("\n");
    }
    for (i = 0; i <= 2; i++) {
        for (j = 0; j <= i; j++) {
            printf(" ");
        }
        for (k = 0; k <= 4 - 2 * i; k++) {
            printf("*");
        }
        printf("\n");
    }

}