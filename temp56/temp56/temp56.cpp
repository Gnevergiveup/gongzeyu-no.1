//录入并保存学生成绩，求解平均分，最高分和最低分
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
    float score[5], sum = 0, max, min,j = 0, n = 0;
    int i;
    for (i = 0; i < 5; i++)
        scanf_s("%f", &score[i]);
    for (i = 0; i < 5; i++)
    {
        j += score[i];
    }
    max = min = score[0];
    for (i = 0; i < 5; i++)
    {
        if (max <= score[i])
            max = score[i];
        if (min >= score[i])
            min = score[i];
    }
    n = j / 5;
    printf("平均分为:%.1f\n", n);
    printf("最高分为:%.1f\n", max);
    printf("最低分为:%.1f\n", min);
}
