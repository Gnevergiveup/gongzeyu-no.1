/*打印出所有的"水仙花数"，所谓"水仙花数"是指一个三位数，其各位数字立方和等于该数 本身。
例如：153是一个"水仙花数"，因为153 = 1的三次方＋5的三次方＋3的三次方*/
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int a=0, b=0, c=0, d=0,count=0;
	for (a = 100; a < 1000; a++)
	{
		b = a % 10;//个位
		c = a / 10 % 10;//十位
		d = a / 100 % 10;//百位
		if (a == b * b * b + c * c * c + d * d * d)
		{
			printf("%5d", a);
			count++;
			if (count % 5 == 0)
				printf("\n");
		}
	}
}