//删除一个字符串中的指定字母，如：字符串 "aca"，删除其中的 a 字母
//1.删除字符串
#define _CRT_SECURE_NO_WARNINGS 1
//错误
//#include<stdio.h>
//#include<string.h>
//void delate(char* x, char* y)
//{
//	int i = 0, j = 0;
//	for (i = 0; i < strlen(x); i++)
//	{
//		if (*(x + i) == *y)
//		{
//			for (j = 1; j < strlen(y); j++)
//			{
//				if (*(y + j) != *(x + i + j))
//				{
//					break;
//				}
//			}
//			if (j == strlen(y))
//			{
//				int t = 0, j = 0;
//				for (j = 0; j < strlen(y); j++)
//				{
//					for (t = i; t < strlen(x); t++)
//					{
//						*(x + t) = *(x + t + 1);
//					}
//				}
//				printf("%s", *x);
//			}
//		}
//	}
//}
//int main(void)
//{
//	char a[256]="/0", b[256]="/0";
//	printf("please input character\n ");
//	gets_s(a);
//	printf("请输入要删除的字符串\n");
//	gets_s(b);
//	delate(a , b);
//}


//正确
#include<stdio.h>
#include<string.h>
int main()
{
	char s1[100];
	gets_s(s1);
	char s2[100];
	gets_s(s2);
	int len1, len2;//字符串长度
	len1 = strlen(s1);
	len2 = strlen(s2);
	int i, j, k;//循环变量
	int flag = 1;//控制while循环
	int f;//判断是否删除
	while (flag)
		for (i = 0; i < len1; i++)
		{
			flag = 0;//默认s1中不存在符合要求的子串，若遍历完后flag仍为0则程序结束 
			if (s1[i] == s2[0])//寻找与子串第一个字母相同的字符 
			{
				f = 1;//默认从第i个字符开始的子串符合要求 
				for (j = i, k = 0; k < len2; j++, k++)//检验是否符合要求
					if (s1[j] != s2[k])//若不符合要求，则f=0,退出for循环的检验
					{
						f = 0;
						break;
					}
				if (f)//若f不为0，则进行删除操作
				{
					flag = 1;//即存在符合要求的子串，将flag=1以便再次检查 
					for (; j < len1; j++, i++)//将后面的字符逐一替换到前面 
						s1[i] = s1[j];
					for (; i < len1; i++)//对后续多余的字符串进行清空 
						s1[i] = '\0';
					break;//重新开始for循环，从第一位开始寻找 
				}
			}
			len1 = strlen(s1);//重新计算s1的长度，此步影响的用时很小，可有可无
		}
	puts(s1);
	return 0;
}

//错误
//#include<stdio.h>
//#include<string.h>
//int main(void)
//{
//	char a[60] = "/0", b[60] = "/0";
//	gets_s(a);
//	gets_s(b);
//	int len1, len2;
//	char* x = a, * y = b;
//	len1 = strlen(a);
//	len2 = strlen(b);
//	int flag = 1;
//	int i = 0, j = 0, t = 0;
//	while (flag)
//	{
//		flag = 0;
//		for (i = 0; i < len1; i++)
//		{
//			if (*(x + i) == *b)
//			{
//				for (j = 0; j < len2; j++)
//				{
//					if (*(x + i + j) != (*(y + j)))
//					{
//						break;
//					}
//					if (j == len2)
//					{
//						flag = 1;
//						for (j=i+1; i < len1; j++, i++)
//						{
//							*(x + i) = *(x + j);
//						}
//					}
//				}
//			}
//		}
//	}
//}
