//学习static的另一个用法：分析下列程序
#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
    int i, num;
    num = 2;//定义变量
    for (i = 0; i < 3; i++)
    {
        printf("num 变量为 %d \n", num);//输出2
        num++;//num=3，num=4
        {
            static int num = 1;
            printf("内置模块 num 变量为 %d\n", num);
            num++;//num=2 
        }
    }
    return 0;
}
//输出 2 1 3 2 4 3